﻿# enezKreiz

[en]
Main service for configuring "enez" system, and to retrieve, save and plot data without the need of an Internet connexion.

[fr]
Service central permettant de configurer le system "enez", et ensuite de récupérer, enregistrer et tracer ses données sans nécessiter de connection Internet. 