#!/usr/bin/python
# -*- coding: utf-8 -*-

from app import app
from flask import render_template, request, abort, redirect, url_for, jsonify
import simplejson as json
import psycopg2 as psql

from app.models import *
from app.enezMqtt import *


###########################################################
# Utils
#
def safeCast(val, toType, default=None):
    try:
        return toType(val)
    except expression as identifier:
        return default

def isMobile():
    if not 'mobile' in g:
        ua = request.headers.get('User-Agent')
        mob = False
        if ('Android' in ua):
            mob = True
        g.mobile = mob
    return


###########################################################
# Screens's routes
#
@app.route('/favicon.ico')
def favicon():
    """ favicon"""
    return app.send_static_file('favicon.ico')

#@app.route('/base')
#def base():
#    """ Base page display
#    """
#    loadSettings()
#    isMobile()
#    return render_template('base.html')

@app.route('/')
def main():
    """ Main page display
    """
    loadSettings()
    isMobile()
    #print(flask.request.host)
    return render_template('main.html')

@app.route('/menu')
def menuPage():
    loadSettings()
    isMobile()
    return render_template('menuPage.html')

@app.route('/admin')
def adminPage():
    loadSettings()
    isMobile()
    return render_template('admin.html')

@app.route('/to_do')
def to_doPage():
    loadSettings()
    isMobile()
    return render_template('to_do.html')

@app.route('/help')
def help():
    loadSettings()
    isMobile()
    return render_template('help.html')

@app.route('/haltDiaz')
def haltDiaz():
    sendToMqtt('enez/haltDiaz', 'Do', 'DoIt')
    return redirect(url_for('main'))

@app.route('/streamList', methods=['GET'])
def streamList():
    loadSettings()
    isMobile()
    return render_template('streamList.html')

@app.route('/streamEdit/<int:id>', methods=['GET', 'POST'])
@app.route('/streamEdit/new', methods=['GET', 'POST'])
def streamEdit(id=-1):
    """Entering new stream and modifying an existing stream
    """
    return coreStreamEdit(id=id, flavor='')

@app.route('/cardList', methods=['GET'])
def cardList():
    """ List of cards displayed on the dashboard.
    """
    loadSettings()
    isMobile()
    return render_template('cardList.html')

@app.route('/cardEdit/<int:id>', methods=['GET', 'POST'])
@app.route('/cardEdit/new', methods=['GET', 'POST'])
def cardEdit(id=-1):
    """ Enter and edit a dashboard card
    """
    loadSettings()
    isMobile()
    if (request.method == 'GET'):  
        # display
        return render_template('cardEdit.html', id=id, crd=cardFromID(id)[1])
    else:  
        print(request)
        # Save card : recover parameters
        id = safeCast(request.form['id'], int, None)
        print(id)
        crd = {}  
        dat = {}
        crd['title'] = request.form['title']
        b = request.form['backColor'].strip()
        if (len(b) == 0):
            crd['backColor'] = '#E2B8E9'
        else:
            crd['backColor'] = b  # ToDo: Checking color
        crd['yLabel'] = request.form['yLabel']
        d = request.form['days'].strip()
        d = d.replace('[', '').replace(']', '')
        x = []
        for  i in d.split(','):
            i = i.strip()
            if i.isdigit():
                x.append(int(i))
        crd['days'] = x
        dat['stream'] = request.form['stream']
        dat['elem'] = request.form['elem']
        dat['unit'] = request.form['unit']
        n = request.form['dec']
        if (n.isdigit()):
            dat['dec'] = n
        else:
            dat['dec'] = 0
        dat['graphType'] = request.form['graphType']
        crd['data'] = [dat] 
        print(crd)
        paramUpsert('card', id, crd)
        sendToMqtt('enez/updateCard', 'now', datetime.datetime.now().isoformat())
        return redirect(url_for('cardList'))

@app.route('/cardDel/<int:id>', methods=['GET'])
def cardDel(id):
    """ Delete card
    """
    cardDelete(id)
    return redirect(url_for('cardList'))


@app.route('/mqttEdit', methods=['GET', 'POST'])
def mqttEdit():
    """ Entering and modifying the MQTT settings
    """
    return coreMqttEdit()

@app.route('/graph/<int:id>')
def graph(id):
    loadSettings()
    isMobile()
    for crd in g.cards:
        if (crd[0] == id):
            break
    return render_template('graph.html', crd=crd[1])

@app.route('/graph1/<title>/<code>/<elem>/<days>/<unit>/<ylabel>')
def graph1(title, code, elem, days, unit, ylabel):
    loadSettings()
    isMobile()
    return render_template('graph.html', title=title, code=code, elem=elem, days=days, unit=unit, ylabel=ylabel)

@app.route('/graphDyn/<title>/<code>/<elem>/<days>/<unit>/<ylabel>')
def graphDyn(title, code, elem, days, unit, ylabel):
    loadSettings()
    isMobile()
    return render_template('graphDyn.html', title=title, code=code, elem=elem, days=days, unit=unit, ylabel=ylabel)


###########################################################
# Node-red pages routes
#

@app.route('/nrBase')
def nrBase():
    """ Node-red Base page display
    """
    loadSettings()
    isMobile()
    return render_template('nodeRed/nrBase.html')

@app.route('/nrStreamList', methods=['GET'])
def nrStreamList():
    """ Display the list of streams without header & footers, for Node-red screen
    """
    loadSettings()
    isMobile()
    return render_template('nodeRed/nrStreamList.html')

@app.route('/nrStreamEdit/<int:id>', methods=['GET', 'POST'])
@app.route('/nrStreamEdit/new', methods=['GET', 'POST'])
def nrStreamEdit(id=-1):
    """ Entering new stream or modifying an existing stream for Node-red screen
    """
    return coreStreamEdit(id=id, flavor='nr')

@app.route('/nrMqttEdit', methods=['GET', 'POST'])
def nrMqttEdit():
    """ Entering and modifying the MQTT settings
    """
    return coreMqttEdit(flavor='nr')


###########################################################
# core screens
#

def coreStreamEdit(id=-1, flavor=''):
    """ Entering new stream or modifying an existing stream - core version
    """
    editTpl = 'streamEdit.html'
    listTpl = 'streamList'
    if (flavor == 'nr'):
        editTpl = 'nodeRed/nrStreamEdit.html'
        listTpl = 'nrStreamList'
    loadSettings()
    isMobile()
    if (request.method == 'GET'):  
        print("BBB_1")
        # Display stream
        row = streamFromID(id)
        print(editTpl)
        return render_template(editTpl, id=id, code=row[1], name=row[2], desc=row[3], topic=(row[4] or ''), rules=(row[5] or ''), lastValue=(row[6] or '.'))
    else:  
        # Save stream : recover parameters
        id = safeCast(request.form['id'], int, None)
        rules = request.form['rules']
        try:
            jrules = json.loads(rules)
        except:
            jrules = None
        streamUpsert(id, request.form['code'], request.form['name'], request.form['desc'], request.form['topic'], request.form['rules'])
        # Send to MQTT information about updating data streams
        sendToMqtt('enez/updateFlux', 'now', datetime.datetime.now().isoformat()) #ToDo a faire en arrière plan
        return redirect(url_for(listTpl))

def coreMqttEdit(flavor=''):
    """ Entering and modifying the MQTT settings - core version
    """
    mqttTpl = 'configMqtt.html'
    rtnTpl = 'main'
    if (flavor == 'nr'):
        mqttTpl = 'nodeRed/nrConfigMqtt.html'
        rtnTpl = 'nrMqttEdit'
    loadSettings()
    isMobile()
    paramCode = 'mqtt'
    if (request.method == 'GET'):  
        # display MQTT parameters
        cnf = getParam(paramCode)
        print('Config_MQTT')
        print(cnf)
        return render_template(mqttTpl, conf=cnf)
    else:  
        # Save parameter : recover parameters
        paramValue = {}
        try:
            paramValue['broker'] = request.form['broker']
            paramValue['port'] = request.form['port']
            paramValue['socketport'] = request.form['socketport']
            paramValue['user'] = request.form['user']
            paramValue['pwd'] = request.form['pwd']
            print(paramValue)
        except Exception as ex:
            print("ERROR")
        paramUpsert(paramCode, None, paramValue)
        return redirect(url_for(rtnTpl))


###########################################################
# APIs's routes
#
@app.route('/csv/<code>/<elem>/<days>/<unit>')
def csvRoute(code, elem, days, unit):
    """ Returns historical data in .csv format
    """
    return sqlCsv(code, elem, days, unit)

@app.route('/c:sv/<code>/<elem>/<days>/<unit>')
def csvRoute2(code, elem, days, unit):
    """ Returns historical data in .csv format
    """
    return sqlCsv(code, elem, days, unit)

@app.route('/csv2/<code>/<elem>/<days>/<unit>')
def csv2Route(code, elem, days, unit):
    """ To be done...
    """
    return 'query = ' + code + ' = ' + elem + ' = ' + days + ' = ' + unit

@app.route('/csvFn/<function>/<code>/<elem>/<days>/<interval>/<unit>')
def csvRouteFn(function, code, elem, days, interval, unit):
    """ Returns historical data in .csv format
    """
    return sqlCsvFn(function, code, elem, days, interval, unit)

@app.route('/csv2Fn/<function>/<code1>/<elem1>/<unit1>/<code2>/<elem2>/<unit2>/<days>/<interval>')
def csv2RouteFn(function, code1, elem1, unit1, code2, elem2, unit2, days, interval):
    """ Returns historical data in .csv format
    """
    return sqlCsv2Fn(function, code1, elem1, unit1, code2, elem2, unit2, days, interval)

@app.route('/nrHisto/<code>/<elem>/<days>/<unit>')
def jsonRoute(code, elem, days, unit):
    """ Returns historical data in json format
    """
    return jsonify(sqlNodeRedHisto(code, elem, days, unit))

@app.route('/flux')
def flux():
    """ Returns information about the streams managed by "enezKreiz"
    """
    return jsonify(topics=sqlFlux()[0])
    
