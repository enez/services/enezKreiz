#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
[en]
PostgreSQL scripts for creating tables that must be present in the database.
Table scripts are versioned to avoid corrupting or losing data during updates.

[fr]
Scripts PostgreSQL de création des tables qui doivent être présentes dans la base de données.
Les scripts de tables sont versionnées dans l'optique de ne pas corrompre ou perdre des données lors des mises à jour.
"""

sqlVersionTables = (
( 0, """
		/* 
		** Table public.streams
		*/
				CREATE TABLE public.streams (
					strm_id bigserial NOT NULL, -- Clé primaire
					strm_desc varchar(2000) NULL, -- Description de l'appareil émetteur de données.
					strm_code varchar(20) NOT NULL, -- Code de l'appareil émetteur de données. Ce code devra être fourni pour qiue les données soient enregistrées. par défaut, ce sera un numero d'ordre
					strm_name varchar(100) NULL, -- Nom de l'appareil fournisseur de données. Le lieu peut y être ajouté afin de l'identifier rapidement.
					strm_topic varchar(2000) NULL, -- Topic MQTT d'origine
					strm_rules json NULL, -- Règles et conditions à respecter pour enregistrer la données en base.
					CONSTRAINT strm_pk PRIMARY KEY (strm_id)
				);
				COMMENT ON TABLE public.streams IS 'Table permettant de lister les différents appareils d''un client, capables d''envoyer des données vers la base.';

				-- Column comments

				COMMENT ON COLUMN public.streams.strm_id IS 'Clé primaire';
				COMMENT ON COLUMN public.streams.strm_desc IS 'Description de l''appareil émetteur de données.';
				COMMENT ON COLUMN public.streams.strm_code IS 'Code de l''appareil émetteur de données. Ce code devra être fourni pour qiue les données soient enregistrées. par défaut, ce sera un numero d''ordre';
				COMMENT ON COLUMN public.streams.strm_name IS 'Nom de l''appareil fournisseur de données. Le lieu peut y être ajouté afin de l''identifier rapidement.';
				COMMENT ON COLUMN public.streams.strm_topic IS 'Topic MQTT d''origine';
				COMMENT ON COLUMN public.streams.strm_rules IS 'Règles et conditions à respecter pour enregistrer la données en base.';

				-- Permissions

				ALTER TABLE public.streams OWNER TO "devPi";
				GRANT ALL ON TABLE public.streams TO "devPi";
""")
,
( 0, """
		/* 
		** Table public.process
		*/
				CREATE TABLE public.process (
					proc_id bigserial NOT NULL,
					proc_strm_id int8 NOT NULL, -- Lien vers l'appareil émetteur de ces données
					proc_code varchar(20) NOT NULL DEFAULT 'raw'::character varying, -- Le code permet de nommer le processus. Par défaut, le code est "raw", qui correspond aux données brutes n'ayant subies aucun traitement.
					proc_last_ts timestamp NULL, -- Il s'agit du dernier timestamp du process source qui a été traité. Cela permet de ne pas retraiter ce qui a déjà été traité. Cette colonne est sans objet pour un processus "raw".
					proc_desc varchar(2000) NULL, -- Description du processus.
					proc_timing varchar(100) NULL, -- Intervalle de temps, exprimé en minutes, ou horaires utilisés pour déclencher le traitement ou l'agrégation des données. Cette colonne est sans objet pour un processus "raw".
					proc_sql varchar(2000) NULL, -- Code SQL à éxécuter par ce processus
					CONSTRAINT proc_pk PRIMARY KEY (proc_id),
					CONSTRAINT proc_chk_strm FOREIGN KEY (proc_strm_id) REFERENCES streams(strm_id) MATCH FULL ON DELETE RESTRICT
				);
				CREATE UNIQUE INDEX proc_idx_unique ON public.process USING btree (proc_strm_id, proc_code);
				COMMENT ON INDEX public.proc_idx_unique IS 'Index unique pour accès aux données';
				COMMENT ON TABLE public.process IS 'Un processus permet de définir un traitement appliqué automatiquement aux données, afin par exemple d''agréger ces données et d''en obtenir des statistiques ou des valeurs par interval de temps.';

				-- Column comments

				COMMENT ON COLUMN public.process.proc_strm_id IS 'Lien vers l''appareil émetteur de ces données';
				COMMENT ON COLUMN public.process.proc_code IS 'Le code permet de nommer le processus. Par défaut, le code est "raw", qui correspond aux données brutes n''ayant subies aucun traitement.';
				COMMENT ON COLUMN public.process.proc_last_ts IS 'Il s''agit du dernier timestamp du process source qui a été traité. Cela permet de ne pas retraiter ce qui a déjà été traité. Cette colonne est sans objet pour un processus "raw".';
				COMMENT ON COLUMN public.process.proc_desc IS 'Description du processus.';
				COMMENT ON COLUMN public.process.proc_timing IS 'Intervalle de temps, exprimé en minutes, ou horaires utilisés pour déclencher le traitement ou l''agrégation des données. Cette colonne est sans objet pour un processus "raw".';
				COMMENT ON COLUMN public.process.proc_sql IS 'Code SQL à éxécuter par ce processus';

				-- Permissions

				ALTER TABLE public.process OWNER TO "devPi";
				GRANT ALL ON TABLE public.process TO "devPi";
""")
,
( 0, """
		/* 
		** Table public.frames
		*/
				CREATE TABLE public.frames (
					fram_id bigserial NOT NULL, -- Clé primaire
					fram_proc_id int8 NOT NULL, -- Lien vers le processus générateur de ces données.
					fram_ts timestamp NOT NULL, -- Heure de réception et d'enregistrement des données.
					fram_json json NULL, -- Représentation jSon des données reçues, que l'on doit retrouver réparties dans la table "singles"
					CONSTRAINT fram_pk PRIMARY KEY (fram_id),
					CONSTRAINT fram_chk_proc FOREIGN KEY (fram_proc_id) REFERENCES process(proc_id) MATCH FULL ON DELETE RESTRICT
				);
				CREATE UNIQUE INDEX fram_idx_unique ON public.frames USING btree (fram_proc_id, fram_ts);
				COMMENT ON TABLE public.frames IS 'Chaque réception de données provoque l''enregistrement d''un frame.';

				-- Column comments

				COMMENT ON COLUMN public.frames.fram_id IS 'Clé primaire';
				COMMENT ON COLUMN public.frames.fram_proc_id IS 'Lien vers le processus générateur de ces données.';
				COMMENT ON COLUMN public.frames.fram_ts IS 'Heure de réception et d''enregistrement des données.';
				COMMENT ON COLUMN public.frames.fram_json IS 'Représentation jSon des données reçues, que l''on doit retrouver réparties dans la table "singles"';

				-- Constraint comments

				COMMENT ON CONSTRAINT fram_pk ON public.frames IS 'clé primaire';

				-- Permissions

				ALTER TABLE public.frames OWNER TO "devPi";
				GRANT ALL ON TABLE public.frames TO "devPi";
""")
,
( 0, """
		/* 
		** Table public.singles
		*/
				CREATE TABLE public.singles (
					sng_fram_id int8 NOT NULL, -- Lien vers le frame à l'origine de cet enregistrement.
					sng_code varchar(20) NOT NULL, -- Code identifiant le capteur. Avec le framID, il constitue la clé primaire.
					sng_val float8 NOT NULL, -- Valeur lue par le capteur
					CONSTRAINT sng_pk PRIMARY KEY (sng_fram_id, sng_code),
					CONSTRAINT sng_chk_fram FOREIGN KEY (sng_fram_id) REFERENCES frames(fram_id) MATCH FULL
				);
				CREATE UNIQUE INDEX sng_idx_unique ON public.singles USING btree (sng_fram_id, sng_code);
				COMMENT ON INDEX public.sng_idx_unique IS 'index sur la clé primaire';
				COMMENT ON TABLE public.singles IS 'chaque frame est décomposé en valeurs élémentaires, afin d''en facilité la gestion par la base de données.';

				-- Column comments

				COMMENT ON COLUMN public.singles.sng_fram_id IS 'Lien vers le frame à l''origine de cet enregistrement.';
				COMMENT ON COLUMN public.singles.sng_code IS 'Code identifiant le capteur. Avec le framID, il constitue la clé primaire.';
				COMMENT ON COLUMN public.singles.sng_val IS 'Valeur lue par le capteur';

				-- Constraint comments

				COMMENT ON CONSTRAINT sng_pk ON public.singles IS 'Clé primaire';

				-- Permissions

				ALTER TABLE public.singles OWNER TO "devPi";
				GRANT ALL ON TABLE public.singles TO "devPi";
""")
,
( 0, """
		/* 
		** Table public.sensors
		*/
				CREATE TABLE public.sensors (
					sens_id bigserial NOT NULL,
					sens_strm_id int8 NOT NULL, -- Lien vers le stream. cela permet d'avoir une description des capteurs que possède un appareil
					sens_code varchar(20) NOT NULL,
					sens_unit varchar(20) NULL, -- Unité de la valeur renvoyée par ce capteur
					sens_desc varchar(2000) NULL, -- Description du capteur
					CONSTRAINT sens_pk PRIMARY KEY (sens_id),
					CONSTRAINT sens_chk_strm FOREIGN KEY (sens_strm_id) REFERENCES streams(strm_id) MATCH FULL ON DELETE RESTRICT
				);
				CREATE UNIQUE INDEX sens_idx_unique ON public.sensors USING btree (sens_strm_id, sens_code);
				COMMENT ON INDEX public.sens_idx_unique IS 'Index unique';
				COMMENT ON TABLE public.sensors IS 'Table répertoriant les capteurs associés à un appareil émetteur de données.';

				-- Column comments

				COMMENT ON COLUMN public.sensors.sens_strm_id IS 'Lien vers le stream. cela permet d''avoir une description des capteurs que possède un appareil';
				COMMENT ON COLUMN public.sensors.sens_unit IS 'Unité de la valeur renvoyée par ce capteur';
				COMMENT ON COLUMN public.sensors.sens_desc IS 'Description du capteur';

				-- Permissions

				ALTER TABLE public.sensors OWNER TO "devPi";
				GRANT ALL ON TABLE public.sensors TO "devPi";
""")
,
( 0.1, """
		/* 
		** Column  strm_topic  in table  public.streams
		*/
				DO
				$do$
				BEGIN
				IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'public' AND TABLE_NAME = 'streams' AND COLUMN_NAME = 'strm_topic') THEN
					ALTER TABLE public.streams ADD strm_topic varchar(2000) NULL;
					COMMENT ON COLUMN public.streams.strm_topic IS 'Topic MQTT d''origine';
				END IF;
				IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'public' AND TABLE_NAME = 'streams' AND COLUMN_NAME = 'strm_rules') THEN
					ALTER TABLE public.streams ADD strm_rules json NULL;
					COMMENT ON COLUMN public.streams.strm_rules IS 'Règles et conditions à respecter pour enregistrer la données en base.';
				END IF;
				END 
				$do$
""")
,
( 0.1, """
		/* 
		** Table public.params
		*/
				CREATE TABLE public.params (
					param_code varchar(50) NOT NULL, -- Code permettant d'identifier le paramètre enregistré. Ce code doit donc être unique.
					param_json jsonb NULL, -- Les paramètres pouvant être composés de plusieurs éléments, ils sont donc encapsulés dans un document json.
					param_ts timestamp NULL DEFAULT now(), -- Date d'enregistrement ou de la dernière modification effectuée sur cet enregistrement. Cette colonn est mise à jour automatiquement par le système.
					CONSTRAINT params_pk PRIMARY KEY (param_code)
				);
				CREATE UNIQUE INDEX params_param_code_idx ON public.params USING btree (param_code);
				COMMENT ON TABLE public.params IS 'Pour enregistrer les différents paramètres de configuration et de fonctionnement des applications ratachés à cette base de données.';

				-- Column comments

				COMMENT ON COLUMN public.params.param_code IS 'Code permettant d''identifier le paramètre enregistré. Ce code doit donc être unique.';
				COMMENT ON COLUMN public.params.param_json IS 'Les paramètres pouvant être composés de plusieurs éléments, ils sont donc encapsulés dans un document json.';
				COMMENT ON COLUMN public.params.param_ts IS 'Date d''enregistrement ou de la dernière modification effectuée sur cet enregistrement. Cette colonn est mise à jour automatiquement par le système.';

				-- Permissions

				ALTER TABLE public.params OWNER TO "devPi";
				GRANT ALL ON TABLE public.params TO "devPi";
""")
,
( 0.11, """
		/* 
		** Table public.process
		*/
				ALTER TABLE public.process ADD proc_param jsonb NULL;
""")
)
