#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
[en]
Class managing version numbers (database, Web,...)
and updating the database according to the following steps:
 -Version 0: table [streams] is not present.
	It is considered that no table is present
 -Version 0.1:
	Table  [streams] is present.
	Tables in version 0.1 have to be added: [params]
 -Version > 0.1:
	It is possible to get the base version in the [params] table
The functions are not linked to a version and will therefore always be added/updated if a version difference is detected.

[fr]
Classe gérant les numéros de versions (database, web, ...)
et la mise à jour de la base de données selon les étapes suivantes :
 - Version 0 : 
 	La table [streams] n'est pas présente.
	On considère alors qu'aucune table n'est présente
 - Version 0.1 :
	La table [streams] est présente.
	Il faut ajouter les tables de la version 0.1 : [params]
 - Version > 0.1 :
	Il est possible d'obtenir la version de la base dans la table [params]
Les fonctions ne sont pas liées à une version et seront donc systématiquement toutes ajoutées / mises à jour 
si une différence de version est constatée.
"""

import simplejson as json
import psycopg2 as psql
import sys
import app.dbTables as dbTables
import app.dbFunctions as dbFunctions

class Versions:
	"""
	[en]
	Class managing version numbers (database, Web,...)
	and updating the database according to the following steps:
	-Version 0: table [streams] is not present.
		It is considered that no table is present
	-Version 0.1:
		Table  [streams] is present.
		Tables in version 0.1 have to be added: [params]
	-Version > 0.1:
		It is possible to get the base version in the [params] table
	The functions are not linked to a version and will therefore always be added/updated if a version difference is detected.

	[fr]
	Classe gérant les numéros de versions (database, web, ...)
	et la mise à jour de la base de données selon les étapes suivantes :
	- Version 0 : 
		La table [streams] n'est pas présente.
		On considère alors qu'aucune table n'est présente
	- Version 0.1 :
		La table [streams] est présente.
		Il faut ajouter les tables de la version 0.1 : [params]
	- Version > 0.1 :
		Il est possible d'obtenir la version de la base dans la table [params]
	Les fonctions ne sont pas liées à une version et seront donc systématiquement toutes ajoutées / mises à jour 
	si une différence de version est constatée.
	"""

	def __init__(self, db):
		"""	Class constructor. 
		    db parameter is mandatory. It defines the database to use.
		"""
		self.db = db


	def setVersion(self, entity, verNum):
		"""	Registers the (n) version of the entity (v)
			example: to set the base version to 0.2 = > setVersion (' dataBase ', 0.2)
		"""
		conn = None
		try:
			conn = self.db
			cur = conn.cursor()

			# get versions
			cur.execute("SELECT param_json FROM params WHERE (param_code = 'versions')")
			vers = cur.fetchone()
			j = {}
			if vers:
				j = vers[0]
			print(j)
			
			# change database version number
			j[entity] = verNum
			query = "INSERT INTO params ( param_code, param_json ) SELECT 'versions' AS code, %(json)s AS param ON CONFLICT (param_code) DO UPDATE SET param_json = EXCLUDED.param_json, param_ts = EXCLUDED.param_ts ;"
			data = {'json':json.dumps(j)}
			cur.execute(query, data)
			conn.commit()
			cur.close()
		except (Exception, psql.Error) as error :
			print('Versions.setVersion:', error)

	def updateFunctions(self):
		""" Update function.
			All functions are updated
		"""
		try:
			conn = self.db
			cur = conn.cursor()
			for initFunction in dbFunctions.sqlVersionFunctions:
				cur.execute(initFunction)
			conn.commit()
			cur.close()
			print("Functions created !")
		except (Exception, psql.Error) as error :
			print('dbVersions.updateFunctions', error)

	
	def updateDb(self):
		""" Updates the database from the existing version to the latest version.
			If an update is required, then updates the functions.
		"""
		#print("updateDb")
		conn = None
		dbUpdate = False
		try:
			conn = self.db
			cur = conn.cursor()

			# First test: presence of tables (table [streams])
			cur.execute("select * from pg_tables where schemaname='public' and tablename='streams'")
			if not cur.fetchone():
				""" It's a blank database. 
					So the creation scripts (version 0) must be run
				""" 
				print("Table [streams] NOT Found !")
				for initTable in dbTables.sqlVersionTables:
					if (initTable[0] == 0):
						cur.execute(initTable[1])
				conn.commit()
				dbUpdate = True
				print("Tables version 0 created !")

			# Second test: presence of table [params]
			cur.execute("select * from pg_tables where schemaname='public' and tablename='params'")
			if not cur.fetchone():
				""" This is an imported database from an earlier version. 
					So the update scripts to version 0.1 must be run
				""" 
				print("Table [params] NOT Found !")
				for initTable in dbTables.sqlVersionTables:
					if (initTable[0] == 0.1):
						cur.execute(initTable[1])
				conn.commit()
				self.setVersion('dataBase', 0.1)
				dbUpdate = True
				print("Tables version 0.1 created !")

			# Third test: version number
			cur.execute("SELECT param_json FROM params WHERE (param_code = 'versions')")
			vj = cur.fetchone()[0]
			for initTable in dbTables.sqlVersionTables:
				if (initTable[0] > vj['dataBase']):
					cur.execute(initTable[1])
					dbUpdate = True
			conn.commit()
			if (dbUpdate == True):
				self.setVersion('dataBase', initTable[0])
				print("Tables to version {0} created !".format(initTable[0]))

			if (dbUpdate == True):
				self.updateFunctions()
			
			conn.commit()
			cur.close()
		except (Exception, psql.Error) as error :
			print(error)

if __name__ == '__main__':
	db = psql.connect(host="127.0.0.1", port=5432, database="enez1", user="devPi", password="dpi")
	Versions(db).updateDb()	
	db.close()
