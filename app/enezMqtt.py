#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
[en]
Functions for sending information to the MQTT service.
It is mainly information about the operation of this service, and allowing other services to be warned.

[fr]
Fonctions permettant de'envoyer des informations au service MQTT.
Ce sont principalement des informations concernant le fonctionnement de ce service, et permettant à d'autres services d'être prévenus.
"""

import paho.mqtt.client as mqtt
import random
import datetime
import json
from app.models import *


def toMqttOnConnect(client, userdata, flags, rc):
	if (rc == 0):
		client.connectedFlag = True
		print('to connection OK')
	else:
		print('to not connected ' + rc)


def sendToMqtt(topic, item, data):
	p = getParam('mqtt') 
	# Client ID must be unique
	clientId = 'enezKreiz' + str(random.randint(10000, 99999))
	mqttC = mqtt.Client(clientId)
	mqttC.on_connect = toMqttOnConnect
	mqttC.connect(p['broker'], int(p['port']))
	msg = {}
	msg[item] = data
	mqttC.publish(topic, json.dumps(msg), retain=False)
	mqttC.disconnect()

