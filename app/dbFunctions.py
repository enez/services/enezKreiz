#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
[en]
PostgreSQL functions used, which must be present in the database.
Functions are not versioned

[fr]
Functions PostgreSQL utilisées, qui doivent être présentes dans la base de données.
Les fonctions ne sont pas versionnées
"""

sqlVersionFunctions = (
"""
/* 
** Function   stream_get(i_id)
*/
		CREATE OR REPLACE FUNCTION public.stream_get(i_id bigint)
		RETURNS TABLE(strm_id bigint, strm_code character varying, strm_name character varying, strm_desc character varying, strm_topic character varying, strm_rules json, fram_json json)
		LANGUAGE plpgsql
		AS $function$
		BEGIN
		RETURN QUERY
		SELECT 
				s.strm_id,
				s.strm_code,
				s.strm_name,
				s.strm_desc,
				s.strm_topic,
				s.strm_rules,
				(	SELECT 
							f.fram_json 
						FROM frames f
							INNER JOIN process p on (p.proc_id = f.fram_proc_id)
						WHERE (p.proc_strm_id = s.strm_id)
							AND (p.proc_code = 'raw')
							AND (f.fram_ts = ( SELECT max(m.fram_ts) FROM frames m WHERE (m.fram_proc_id = p.proc_id) ) )
				) AS fram_json
			FROM streams s
			WHERE (s.strm_id = i_id);
		END;
		$function$ ;
"""
,
"""
/* 
** Function   stream_upsert(i_id, i_code, i_name, i_desc, i_topic, i_rules)
*/
		CREATE OR REPLACE FUNCTION public.stream_upsert(i_id bigint, i_code character varying, i_name character varying, i_desc character varying, i_topic character varying, i_rules json)
		RETURNS bigint
		LANGUAGE plpgsql
		AS $function$
		BEGIN
			IF (i_id < 0) THEN 
				-- C'est un nouvel enregistrement
				INSERT INTO streams ( strm_code, strm_name, strm_desc, strm_topic, strm_rules )
					VALUES ( i_code, i_name, i_desc, i_topic, i_rules )
					RETURNING strm_id INTO i_id;
				INSERT INTO process ( proc_strm_id, proc_code, proc_desc )
					VALUES ( i_id, 'raw', i_desc );
			ELSE
				-- c'est une mise à jour
				UPDATE streams SET 
						strm_code = i_code,
						strm_name = i_name,
						strm_desc = i_desc,
						strm_topic = i_topic,
						strm_rules = i_rules
					WHERE (strm_id = i_id);
			END IF;
			
			INSERT INTO params ( param_code, param_json, param_ts )
				SELECT 
					'streamUpdate' AS code,
					('{"Time": "' || CAST(now() AS TIMESTAMP) || '", "stream_id": ' || i_id || '}'):: json AS param,
					now() AS ts
					ON CONFLICT (param_code)
					DO UPDATE SET
						param_json = EXCLUDED.param_json,
						param_ts = EXCLUDED.param_ts ;
			
			RETURN i_id;
		END ;
		$function$ ;
"""
,
"""
/* 
** Function frame_insert(character varying, json)
*/
		CREATE OR REPLACE FUNCTION public.frame_insert(character varying, json)
		RETURNS boolean
		LANGUAGE plpgsql
		AS $function$
		BEGIN
			-- Insert or Update simple params
			INSERT INTO frames ( fram_proc_id, fram_ts, fram_json ) 
				SELECT 
						p.proc_id,
						now(),
						$2
					FROM process p
						INNER JOIN streams s ON (s.strm_id = p.proc_strm_id)
					WHERE (p.proc_code = 'raw')
						AND (s.strm_code = $1);
			RETURN True;
		END ;
		$function$ ;
"""
,
"""
/* 
** Function frame_insert(timestamp without time zone, character varying, json)
*/
		CREATE OR REPLACE FUNCTION public.frame_insert(timestamp without time zone, character varying, json)
		RETURNS boolean
		LANGUAGE plpgsql
		AS $function$
		BEGIN
			INSERT INTO frames ( fram_proc_id, fram_ts, fram_json ) 
				SELECT 
						p.proc_id,
						$1,
						$3
					FROM process p
						INNER JOIN streams s ON (s.strm_id = p.proc_strm_id)
					WHERE (p.proc_code = 'raw')
						AND (s.strm_code = $2);
			RETURN True;
		END ;
		$function$ ;
"""
,
"""
/*
** Function flux_get()
*/
		CREATE OR REPLACE FUNCTION public.flux_get()
		RETURNS json  
		AS $function$
			SELECT 
					json_agg(d) 
				FROM (
					SELECT 
							s.strm_code AS "code",
							s.strm_name AS "name",
							s.strm_desc AS "description",
							s.strm_topic AS "topic",
							(	SELECT 
										f.fram_json 
									FROM process p 
										INNER JOIN frames f ON (f.fram_proc_id = p.proc_id) 
									WHERE (p.proc_strm_id = s.strm_id) 
									ORDER BY 
										f.fram_ts DESC 
									LIMIT 1 
								) AS "last"
						FROM streams s
					) d 
		$function$
 		LANGUAGE sql;
"""
,
"""
/*
** Function param_upsert(i_what character varying, i_id bigint, i_param json)
**  complex primary key with i_id
*/
		CREATE OR REPLACE FUNCTION public.param_upsert(i_what CHARACTER VARYING, i_id BIGINT, i_param JSON)
		RETURNS BIGINT
		LANGUAGE plpgsql
		AS $function$
			-- Insert or Update params with complex Primary Key
			-- Primaries Keys of 'params' table are VARCHAR. Some with same root must be indexed.
			--  * i_what is the root
			--  * i_id is the index. If the index is negative, the next id will be found
			DECLARE n_id INTEGER;	
			DECLARE n_code CHARACTER VARYING;
			BEGIN
				-- Construction du code selon les différents cas
				IF (i_id IS NULL) THEN
					-- C'est une clé primaire simple
					SELECT i_what INTO n_code;
					SELECT NULL INTO n_id;
				ELSE
					IF (i_id < 0) THEN
						-- C'est un nouvel enregistrement avec une clé complexe
						SELECT COALESCE(( SELECT 1+MAX(SUBSTRING(param_code, LENGTH(i_what)+2)::int) FROM params WHERE (param_code LIKE i_what || '%')	), 0) INTO n_id;
					ELSE 
						-- C'est un enregistrement existant avec une clé complexe
						SELECT i_id INTO n_id;
					END IF;
					SELECT i_what || '_' || n_id INTO n_code;
				END IF;
				
				-- Enregistrement du paramètre
				INSERT INTO params ( param_code, param_json, param_ts )
					SELECT 
							n_code,
							i_param,
							NOW()
						ON CONFLICT (param_code)
						DO UPDATE SET
							param_json = EXCLUDED.param_json,
							param_ts = EXCLUDED.param_ts ;
				
				-- Enregistrement de la trace de la modification
				INSERT INTO params ( param_code, param_json, param_ts )
					SELECT 
							'paramUpdate',
							('{"Time": "' || CAST(NOW() AS TIMESTAMP) || '", "param_code": "' || n_code || '"}')::JSON,
							NOW()
						ON CONFLICT (param_code)
						DO UPDATE SET
							param_json = EXCLUDED.param_json,
							param_ts = EXCLUDED.param_ts ;
				
				RETURN n_id;
			END ;
			$function$
		;
"""
,
"""
/* 
** Function tsRound 
*/
		CREATE OR REPLACE FUNCTION public.tsRound(itv integer, ts timestamp without time zone)
		RETURNS timestamp without time zone
		LANGUAGE plpgsql
		AS $function$
			BEGIN
				RETURN TIMESTAMP WITHOUT TIME ZONE 'epoch' + round(extract(epoch from ts) / itv)*itv * INTERVAL '1 second';  -- DateTime arrondie à 'itv' secondes
			END;
		$function$
		;
"""
,
"""
/* 
** Function tsRoundMinute 
*/
		CREATE OR REPLACE FUNCTION public.tsroundminute(itv integer, ts timestamp without time zone)
		RETURNS timestamp without time zone
		LANGUAGE plpgsql
		AS $function$
			BEGIN
				RETURN DATE_TRUNC('hour', ts) + TRUNC(EXTRACT(minutes FROM ts)/5)*5*'1 minutes'::interval;
			END;
		$function$
		;
"""
,
"""
/*
** Function frame_byinterval(p_stream character varying, p_process character varying, p_elem character varying, p_days integer, p_delta integer)
*/
		CREATE OR REPLACE FUNCTION public.frame_byinterval(p_stream character varying, p_process character varying, p_elem character varying, p_days integer, p_delta integer)
		RETURNS TABLE(ts timestamp without time zone, val double precision)
		LANGUAGE sql
		AS $function$
		WITH 
		allIntervals ( rts ) AS (
		SELECT 
			gs
			FROM GENERATE_SERIES ( 
						now()::date - (p_days * '1 day'::interval), -- - (p_delta * '1 minutes'::interval),
						now(),
						p_delta * '1 minutes'::interval) gs
		),
		usableFrames (rts, ts, idx ) AS (
		SELECT 
			/*public.tsRoundMinute(p_delta, f.fram_ts),*/
			public.tsRound(p_delta, f.fram_ts),
			f.fram_ts,
			(f.fram_json->>p_elem)::integer
			FROM frames f
			INNER JOIN process p ON (p.proc_id = f.fram_proc_id)
			INNER JOIN streams s ON (s.strm_id = p.proc_strm_id)
			WHERE (s.strm_code = p_stream)  
			AND ( ( (p_process IS NULL) AND (p.proc_code = 'raw') ) OR ( (p_process IS NOT NULL) AND (p.proc_code = p_process) ) )
			AND (fram_ts >= (SELECT MIN(rts) FROM allIntervals))
		),
		byInterval ( rn, rts, idx ) AS (
		SELECT 
			row_number() OVER(ORDER BY r.ts ASC),
			r.rts,
			r.idx
			FROM usableFrames r
			INNER JOIN (
				SELECT 
					rts,
					MIN(ts) AS ts
				FROM usableFrames f
				GROUP BY
					rts
				) x ON (r.ts = x.ts) 
			INNER JOIN allIntervals a ON (a.rts = r.rts)
		),
		emptyInterval ( rts ) AS (  
		SELECT 
			a.rts
			FROM allIntervals a
			LEFT OUTER JOIN byInterval d ON (a.rts = d.rts)
			WHERE (d.rn IS NULL)
		),
		intervalBeforeEmpty ( rts, rtsPrec ) AS (
		SELECT
			e.rts,
			( SELECT MAX(d.rts) FROM byInterval d WHERE (d.rts < e.rts) )
			FROM emptyInterval e 
		),
		nbIntervals ( rtsPrec, nbInterv ) AS (
		SELECT
			rtsPrec,
			COUNT(1)
			FROM intervalBeforeEmpty
			GROUP BY rtsPrec
		),
		realConso ( rts, conso ) AS (
		SELECT 
			d1.rts,
			d2.idx - d1.idx AS conso
			FROM byInterval d1
			INNER JOIN byInterval d2 ON (d2.rn = d1.rn + 1)
		),
		updatedConso ( rts, conso ) AS (
		SELECT 
			d.rts,
			r.conso / (n.nbInterv + 1)::INT
			FROM nbIntervals n
			INNER JOIN intervalBeforeEmpty d ON (n.rtsPrec = d.rtsPrec)
			INNER JOIN realConso r ON (r.rts = n.rtsPrec)
		UNION
		SELECT 
			n.rtsPrec,
			r.conso / (n.nbInterv + 1)::INT
			FROM nbIntervals n
			INNER JOIN realConso r ON (r.rts = n.rtsPrec)
		)
		SELECT 
			rts::timestamp without time zone AS "ts",
			conso::float AS "val"
		FROM realConso r
		WHERE (r.rts NOT IN ( SELECT rts FROM updatedConso) )
		UNION
		SELECT 
			rts::timestamp without time zone AS "ts",
			conso::float AS "val"
		FROM updatedConso
		ORDER BY 
			ts;
		$function$
		;
"""


)
