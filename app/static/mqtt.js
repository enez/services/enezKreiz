var clientID =  "web_" + parseInt(Math.random() * 1000, 10);
var mqtt = new Array();
var reconnectTimeout = 2000;

function mqConnect() {
  // Do connection
  for(var brk in mqBrokers) {
    mqtt[brk] = new Messaging.Client( mqBrokers[brk]['broker'], mqBrokers[brk]['port'], clientID );
    options = {
      timeout: 3,
      useSSL: false,
      cleanSession: true,
      onSuccess: onConnect,
      onFailure: function (message) {
        $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
        setTimeout(mqConnect, reconnectTimeout);
      }
    };
    mqtt[brk].onConnectionLost = onConnectionLost;
    mqtt[brk].onMessageArrived = onMessageArrived;
    mqtt[brk].connect(options);
  }
};

function onConnect() {
  // Connection succeeded; subscribe to each topics
  for (t in mqFlux) {
    mqtt[mqFlux[t]['broker']].subscribe(mqFlux[t]['topic'], {qos: 0});
  }
};

function onConnectionLost(response) {
  setTimeout(mqConnect, reconnectTimeout);
};

function onMessageArrived(message) {
  // Display message
  var topic = message.destinationName;
  var payload = message.payloadString;
  var obj = JSON.parse(payload);
  // Message from which flux
  for (flu in mqFlux) {
    if (mqFlux[flu]['topic'] == topic) {
      // Browse all relevant Disp
      for (dsp in mqDisp) {
        // and find the concerned one
        if (mqDisp[dsp]['flux'] == flu) {
          // Display part of message
          if (/[^\(]*(\(.*\))[^\)]*/.test(mqDisp[dsp]['elem'])) {
            // tasmota sub document. element represented by parentesis
            var elem1 = mqDisp[dsp]['elem'].replace(/\(\S*\)/gm, "");
            var elem2 = mqDisp[dsp]['elem'].replace(/\S*\(/gm, "").replace(/\)\S*/gm, "");
            x = "$('#" + dsp + "').text(obj." + elem1 + "." + elem2 + ".toFixed(" + mqDisp[dsp]['dec'] + "))";
          }
          else {
            // no sub document
            x = "$('#" + dsp + "').text(obj." + mqDisp[dsp]['elem'] + ".toFixed(" + mqDisp[dsp]['dec'] + "))";
          }
          eval(x);
        }
      }
    }
  }
};

$(document).ready(function() {
  mqConnect();
});

