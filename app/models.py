#!/usr/bin/python
# -*- coding: utf-8 -*-

from app import app
from flask import g
import simplejson as json
import psycopg2 as psql
import app.dbVersion as dbVersion
import re
import datetime


###########################################################
# database access
#
def db():
  """ database connection """
  if not 'bdd' in g:
    try:
      cfg = app.config['DB_PARAMS']
      #print(cfg)
      g.bdd = psql.connect(
                        host = cfg['host'],
                        port = cfg['port'],
                        database = cfg['base'],
                        user = cfg['user'],
                        password = cfg['pwd'] 
                        )
      dbVersion.Versions(g.bdd).updateDb()	
    except Exception as ex:
      print("DataBase unreachable")
      print(ex)
  print(g.bdd)
  return g.bdd


###########################################################
# Globals variables
#

def loadSettings():
  if not 'settings' in g:
    g.settings = app.config['SETTINGS']  # personnal settings
  if not 'versions' in g:
    g.versions = getParam('versions')
  if not 'cards' in g:
    g.cards = cards()
    # add display element (tasmota)
    for c in g.cards:
      c[1]['data'][0]['dispElem'] = c[1]['data'][0]['elem'].replace('(', '_').replace(')', '_')
  if not 'flux' in g:
    g.flux = streams()
  if not 'mqs' in g:
    g.mqs = mqtts()

###########################################################
# Utilities
#
def getParam(param):
  """ Getting the value of the application setting
  """
  db().rollback()
  cur = db().cursor()
  data = {}
  data["param"] = param
  query = "SELECT param_json FROM params WHERE (param_code = %(param)s)"
  try:
    cur.execute(query, data)
    return cur.fetchone()[0]
  except Exception as ex:
    print("Unable to SELECT parameter %s" % param)
  return None

def mqtts():
  """ Getting the settings of the MQTT servers to monitor.
  """
  mq0 = getParam('mqtt')
  return [('default', mq0)]

###########################################################
# Enregistrement des données
#

def paramUpsert(what, id, params):
  """ Saves or updates a record of the [params] table with a complex key consisting of a root and an index
  """
  query = "SELECT param_upsert( %(what)s, %(id)s, %(param)s )"
  data = {}
  data["what"] = what
  data["id"] = id
  data["param"] = json.dumps(params)
  cur = db().cursor()
  cur.execute(query, data)
  db().commit()
  return cur.rowcount > 0

###########################################################
# Data acces layer : the streams (or flux)
#
def streamNew():
  """ Represents a new record of a stream
  """
  return ( -1, '', '', '', None, None, '-' )

def streams():
  """ Retrieves the full definition of all streams
  """
  strm = {}
  cur = db().cursor()
  query = "SELECT strm_id, strm_code, strm_name, strm_desc, strm_topic, strm_rules FROM streams ORDER BY strm_code"
  try:
    cur.execute(query)
    if (cur.rowcount == 0):
      return None
    if (cur.rowcount > 0):
      rows = cur.fetchall()
      for row in rows:
        r = {}
        r['id'] = row[0]
        r['code'] = row[1]
        r['name'] = row[2]
        r['desc'] = row[3]
        r['topic'] = row[4]
        r['rules'] = row[5]
        strm[row[1]] = r
  except Exception as ex:
    print("Unable to SELECT streams data")
    print(ex)
  return strm

def streamFromID(id):
  """ Retrieves all information from a streams recording, from its primary key 
  """
  assert type(id) == int
  if (id >= 0):
    cur = db().cursor()
    query = "SELECT strm_id, strm_code, strm_name, strm_desc, strm_topic, CAST(strm_rules AS VARCHAR), CAST(fram_json AS VARCHAR) FROM stream_get(%(id)s)"
    try:
      cur.execute(query, {'id':id})
      row = cur.fetchone()
    except Exception as ex:
      print("Unable to SELECT data")
      print(ex)
  else:
    row = streamNew()
  return row

def streamUpsert(id, code, name, desc, topic, rules):
  """ Saves or updates a stream recording
  """
  assert type(id)==int
  cur = db().cursor()
  if not rules:
    t = None
  else:
    j = json.loads(rules)
    t = json.dumps(j)
  q = "SELECT stream_upsert( %(id)s, %(code)s, %(name)s, %(desc)s, %(topic)s, %(iRules)s )"
  data = {'id':id, 'code':code, 'name':name, 'desc':desc, 'topic':topic, 'iRules':t} 
  query = cur.mogrify(q, data)
  #print(query)
  cur.execute(query)
  db().commit()
  return cur.rowcount > 0

###########################################################
# Data acces layer : the cards
#
def cardNew():
  """ 
    [en]
    Represents a new dashboard card.
    A map consists of the following information:
        -id: a system-defined identifier number. -1 if not yet
        -title: what is displayed on the button
        -backColor: button background color
        -days: a table with different periods possible via the menu
        -ylabel: the wording for the data displayed in the graph
        -data: a table with the description of the data to chart: (it is planned to be able to display several curves)
            * stream: the stream code in which to fetch the data
            * elem: the element of the json document to chart
            * unit: unit to be displayed (e.g. '°C')
            * dec: number of interesting decimals
            * graphType: chart type. The following types are possible
                o bar: for histograms
                o step: for a staircase curve
                o smooth: for Bezier curves
            * aggregPeriod: to specify the aggregation period. Will be defined later
            * aggregType: to specify how to aggregate
                o AVG: we make an average
                o counter: we take the difference between the start value of the period and the end value
            * color: chart color
            * displayOptions: a json document describing additional options
            ? display ': ' default '
    Example:
        card_1 : { "title": "test btn", "backColor": "#8C52FA", "days": [1, 7], "yLabel": "Temperature", "data": [{ "code": "test", "elem": "t", "unit": "°C", "dec": 0 }] }

    [fr]
    Représente une nouvelle carte du tableau de bord
    Une carte est composée des informations suivantes :
        - id : Un numéro d'identifiant défini par le système. -1 si n'existe pas encore
        - title : Ce qui est affiché sur le bouton
        - backColor : Couleur de fond du bouton
        - days : Un tableau avec les différentes périodes possible via le menu
        - ylabel : Le libellé concernant la donnée affichée dans le graphique
        - data : Un tableau avec la description des données à grapher: (il est prévu de pouvoir afficher plusieurs courbes)
            * stream : Le code du flux (stream) dans lequel aller chercher les données
            * elem : L'élément du document json à grapher
            * unit : Unité à afficher ('°C' par exemple)
            * dec : Nombre de décimales intéressantes
            * graphType : Type de graphique. Les types suivants sont possibles
                o bar : pour des histogrammes
                o step : pour une courbe en escalier
                o smooth : pour des courbes de bézier
            * aggregPeriod : Pour préciser la période d'agrégation. Sera défini utérieurement
            * aggregType : Pour spécifier comment agréger
                o avg : on fait une moyenne
                o counter : on prend la différence entre la valeur de début de période et la valeur de fin
            * color : Couleur de la courbe
            * displayOptions : Un document json décrivant des options supplémentaires
            ? display':'default'
    Exemple:
        card_1 : { "title": "test btn", "backColor": "#8C52FA", "days": [1, 7], "yLabel": "Temperature", "data": [{ "code": "test", "elem": "t", "unit": "°C", "dec": 0 }] }
  """
  return (-1, {'title':'Nouvelle carte', 'backColor':'#E2B8E9', 'yLabel':'légende', 'days':[1], 'data': [{'stream':'', 'elem':'?', 'unit':'-', 'dec':0 }] })

def cards():
  """ Gets details of all the maps displayed on the dashboard (title, stream, label)
  """
  cur = db().cursor()
  query = "SELECT SUBSTRING(param_code, 6)::INT, param_json FROM params WHERE param_code LIKE 'card_%' ORDER BY param_code"
  try:
    cur.execute(query)
    if (cur.rowcount == 0):
      return None
    if (cur.rowcount > 0):
      return cur.fetchall()
  except Exception as ex:
    print("Unable to SELECT cards data")
    print(ex)
  return None

def cardFromID(id):
  """ Gets all informations from a dashboard card, from its ID 
  """
  assert type(id) == int
  card = ()
  if g.cards:
    for c in g.cards:
      if c[0] == id:
        card = c
        break
  if (card == ()):
    card = cardNew()
  return card

def cardDelete(id):
  """ Delete the card specified by its id
  """
  assert type(id) == int
  cur = db().cursor()
  query = "DELETE FROM params WHERE param_code = 'card_%(id)s'"
  data = { "id":id }
  #print(cur.mogrify(query, data))
  try:
    cur.execute(query, data)
    db().commit()
  except Exception as ex:
    print("Unable to DELETE card")
    print(ex)
  return None


###########################################################
# Data acces layer : historic data
#
def histoRows(code, elem, days, unit):
  """ Gets all rows of historical elements
  """
  cur = db().cursor()
  if (re.match("[^\(]*(\(.*\))[^\)]*", elem)):
    #print("MATCH !") 
    elem2 = re.search("\(\S*\)", elem).group(0)[1:-1]
    elem1 = re.search("\S*\(", elem).group(0)[:-1]
    query = """
      SELECT 
          date_trunc('second', f.fram_ts), 
          (f.fram_json->%(elem1)s->>%(elem2)s)::REAL 
        FROM frames f
          INNER JOIN process p ON (p.proc_id = f.fram_proc_id) AND (p.proc_code = 'raw')
          INNER JOIN streams s ON (s.strm_id = p.proc_strm_id)
        WHERE (s.strm_code = %(code)s)
          AND (f.fram_ts > current_date + CAST(TO_CHAR(-%(days)s-0.01, '990.9') || 'days' AS interval))
          AND ((f.fram_json->%(elem1)s->>%(elem2)s)::REAL IS NOT NULL)
        ORDER BY 
          f.fram_ts ASC
      """
    data = { 'elem1':elem1, 'elem2':elem2, 'code':code, 'days':int(days) }
  else:
    #print("NOT MATCH")  
    query = """
      SELECT 
          date_trunc('second', f.fram_ts), 
          (f.fram_json->>%(elem)s)::REAL 
        FROM frames f
          INNER JOIN process p ON (p.proc_id = f.fram_proc_id) AND (p.proc_code = 'raw')
          INNER JOIN streams s ON (s.strm_id = p.proc_strm_id)
        WHERE (s.strm_code = %(code)s)
          AND (f.fram_ts > current_date + CAST(TO_CHAR(-%(days)s-0.01, '990.9') || 'days' AS interval))
          AND ((f.fram_json->>%(elem)s)::REAL IS NOT NULL)
        ORDER BY 
          f.fram_ts ASC
      """
    data = { 'elem':elem, 'code':code, 'days':int(days) }

  #print(cur.mogrify(query, data))
  try:
    cur.execute(query, data)
    rows = cur.fetchall()
    #return()
  except Exception as ex:
    print("Unable to SELECT data")
    rows = None
  cur.close()
  return(rows)

def sqlCsv(code, elem, days, unit):
  """ Gets historical elements in the .csv format
  """
  r = "date,{0}\n".format(unit)
  #cur = db().cursor()
  #if (re.match("[^\(]*(\(.*\))[^\)]*", elem)):
  #  #print("MATCH !") 
  #  elem2 = re.search("\(\S*\)", elem).group(0)[1:-1]
  #  elem1 = re.search("\S*\(", elem).group(0)[:-1]
  #  query = """
  #    SELECT 
  #        date_trunc('second', f.fram_ts), 
  #        (f.fram_json->%(elem1)s->>%(elem2)s)::REAL 
  #      FROM frames f
  #        INNER JOIN process p ON (p.proc_id = f.fram_proc_id) AND (p.proc_code = 'raw')
  #        INNER JOIN streams s ON (s.strm_id = p.proc_strm_id)
  #      WHERE (s.strm_code = %(code)s)
  #        AND (f.fram_ts > current_date + CAST(TO_CHAR(-%(days)s-0.01, '990.9') || 'days' AS interval))
  #        AND ((f.fram_json->%(elem1)s->>%(elem2)s)::REAL IS NOT NULL)
  #      ORDER BY 
  #        f.fram_ts ASC
  #    """
  #  data = { 'elem1':elem1, 'elem2':elem2, 'code':code, 'days':int(days) }
  #else:
  #  #print("NOT MATCH")  
  #  query = """
  #    SELECT 
  #        date_trunc('second', f.fram_ts), 
  #        (f.fram_json->>%(elem)s)::REAL 
  #      FROM frames f
  #        INNER JOIN process p ON (p.proc_id = f.fram_proc_id) AND (p.proc_code = 'raw')
  #        INNER JOIN streams s ON (s.strm_id = p.proc_strm_id)
  #      WHERE (s.strm_code = %(code)s)
  #        AND (f.fram_ts > current_date + CAST(TO_CHAR(-%(days)s-0.01, '990.9') || 'days' AS interval))
  #        AND ((f.fram_json->>%(elem)s)::REAL IS NOT NULL)
  #      ORDER BY 
  #        f.fram_ts ASC
  #    """
  #  data = { 'elem':elem, 'code':code, 'days':int(days) }

  ##print(cur.mogrify(query, data))
  try:
  #  cur.execute(query, data)
  #  rows = cur.fetchall()
    rows = histoRows(code, elem, days, unit)
    for row in rows:
      r += "{0},{1}\n".format(row[0], row[1])
    #cur.close()
    return r[:-1]
  except Exception as ex:
    #print("Unable to SELECT data")
    return None


def sqlCsvFn(function, code, elem, days, interval, unit):
  """ Gets historical elements in the .csv format, from an SQL function
  """
  r = "date,{0}\n".format(unit)
  cur = db().cursor()
  query = """
    SELECT 
        ts,
        val
      FROM {0}(%(code)s, NULL, %(elem)s, %(days)s, %(interval)s)
    """.format(function)
  data = { 'code':code, 'elem':elem, 'days':int(days), 'interval':int(interval) }
  #print(cur.mogrify(query, data))
  try:
    cur.execute(query, data)
    rows = cur.fetchall()
    for row in rows:
      r += "{0},{1}\n".format(row[0], row[1])
    cur.close()
    return r[:-1]
  except Exception as ex:
    print("Unable to SELECT data")
    return None


def sqlCsv2Fn(function, code1, elem1, unit1, code2, elem2, unit2, days, interval):
  """ Gets historical elements in the .csv format, for 2 series, from an SQL function
  """
  r = "date,{0},{1}\n".format(unit1.replace("_", " "), unit2.replace("_", " "))
  #interval = 60
  cur = db().cursor()
  query = """
    SELECT 
        x.ts,
        x.val,
        --y.val
        ABS(y.val) * 1000
      FROM {0}(%(code1)s, NULL, %(elem1)s, %(days)s, %(interval)s) x
        INNER JOIN {0}(%(code2)s, NULL, %(elem2)s, %(days)s, %(interval)s) y ON (x.ts = y.ts)
    """.format(function)
  data = { 'code1':code1, 'elem1':elem1, 'code2':code2, 'elem2':elem2, 'days':int(days), 'interval':int(interval) }
  #print("@@@@@@@@@@@@")
  #print(cur.mogrify(query, data))
  try:
    cur.execute(query, data)
    rows = cur.fetchall()
    for row in rows:
      r += "{0},{1},{2}\n".format(row[0], row[1], row[2])
    cur.close()
    return r[:-1]
  except Exception as ex:
    print("Unable to SELECT data")
    return None


def sqlNodeRedHisto(code, elem, days, unit):
  """ Gets historical elements in the json format for Node-red graphics
  """
  r = [{}]
  r[0]["series"] = [unit]
  r[0]["labels"] = [""]
  data = [[]]
  try:
    rows = histoRows(code, elem, days, unit)
    for row in rows:
      e = {}
      #e["x"] = (row[0]-datetime.datetime(1970,1,1)).total_seconds()
      e["x"] = row[0]
      e["y"] = row[1]
      data[0].append(e)
    r[0]["data"] = data
    return r
  except Exception as ex:
    return None

def sqlJson1(code, elem, days, unit):
  """ Gets historical elements in the json format
  """
  cur = db().cursor()
  if (re.match("[^\(]*(\(.*\))[^\)]*", elem)):
    print("MATCH !") 
    elem2 = re.search("\(\S*\)", elem).group(0)[1:-1]
    elem1 = re.search("\S*\(", elem).group(0)[:-1]
    query = """
      SELECT 
          json_agg(j)
        FROM (
            SELECT 
                json_agg(q.ts) AS "date",
                json_agg(q.val) AS "{0}"
              FROM (
                SELECT 
                    date_trunc('second', f.fram_ts) AS "ts", 
                    (f.fram_json->%(elem1)s->>%(elem2)s)::REAL  AS "val"
                  FROM frames f
                    INNER JOIN process p ON (p.proc_id = f.fram_proc_id) AND (p.proc_code = 'raw')
                    INNER JOIN streams s ON (s.strm_id = p.proc_strm_id)
                  WHERE (s.strm_code = %(code)s)
                    AND (f.fram_ts > current_date + CAST(TO_CHAR(-%(days)s-0.01, '990.9') || 'days' AS interval))
                    AND ((f.fram_json->%(elem1)s->>%(elem2)s)::REAL IS NOT NULL)
                  ORDER BY 
                    f.fram_ts ASC
                ) q
            ) j
      """
    data = { 'elem1':elem1, 'elem2':elem2, 'code':code, 'days':int(days) }
  else:
    query = """
      SELECT 
          json_agg(j)
        FROM (
            SELECT 
                json_agg(q.ts) AS "date",
                json_agg(q.val) AS "{0}"
              FROM (
                SELECT 
                    date_trunc('second', f.fram_ts) AS "ts", 
                    (f.fram_json->>%(elem)s)::REAL AS "val"
                  FROM frames f
                    INNER JOIN process p ON (p.proc_id = f.fram_proc_id) AND (p.proc_code = 'raw')
                    INNER JOIN streams s ON (s.strm_id = p.proc_strm_id)
                  WHERE (s.strm_code = %(code)s)
                    AND (f.fram_ts > current_date + CAST(TO_CHAR(-%(days)s-0.01, '990.9') || 'days' AS interval))
                    AND ((f.fram_json->>%(elem)s)::REAL IS NOT NULL)
                  ORDER BY 
                    f.fram_ts ASC
                ) q
            ) j
      """.format(unit[:5])
    data = { 'elem':elem, 'code':code, 'days':int(days) }
  #print(cur.mogrify(query, data) )
  try:
    cur.execute(query, data)
    return cur.fetchone()
  except Exception as ex:
    print("Unable to SELECT data")
    return None

def sqlFlux():
  """ Gets detailed list of streams managed by the system.
  """
  cur = db().cursor()
  query = """SELECT flux_get() AS "topics" """
  try:
    cur.execute(query)
    return cur.fetchone()
  except Exception as ex:
    print("Unable to SELECT flux")
    return None


