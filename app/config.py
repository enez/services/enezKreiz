#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
[en]
File configuration for  "enezKreiz"
"enezKreiz" is a web service serving pages that allow you to
 - Defining MQTT services to use
 - Configuring the data that the enezDiaz service will use for persisting in database, data provided by the MQTT service defined above
 - Configuring the data that will be accessible on the home screen and define how this data will be displayed

[fr]
Fichier de configuration du soft  "enezKreiz"
"enezKreiz" est un service web servant les pages permettant de:
 - Définir le service MQTT à utiliser
 - Configurer les données que le service enezDiaz sera chargé de persister en base, données fournies par le service MQTT défini ci-dessus
 - Configurer les données qui seront accessibles sur l'écran d'accueil et définir la façon dont ces données seront affichées
"""

# Some general options:
#  - name: name displayed in the page header
#  - noEdit: if 'true', configuration changes can't be updated
#  - mqtt: if 'auto', mqtt broker is the web server, not the one define in database 
SETTINGS = { 'name':'Develop', 'noEdit':'false', 'mqtt':'!auto' }

# Connection string
DB_PARAMS = { 'host':'172.17.7.12', 'port':5432, 'base':'piBase', 'user':'devPi', 'pwd':'KALpg2' }  # enezKal à Kerdale 
#DB_PARAMS = { 'host':'192.168.117.1', 'port':5432, 'base':'PiBase', 'user':'devPi', 'pwd':'OEeu' }
#DB_PARAMS = { 'host':'127.0.0.1', 'port':5432, 'base':'piBase', 'user':'devPi', 'pwd':'dpi' }
#DB_PARAMS = { 'host':'192.168.0.105', 'port':5432, 'base':'piBase', 'user':'devPi', 'pwd':'dpi' }
#DB_PARAMS = { 'host':'192.168.0.15', 'port':5432, 'base':'piBase', 'user':'devPi', 'pwd':'dpi' } 
#DB_PARAMS = { 'host':'192.168.1.201', 'port':5432, 'base':'piBase', 'user':'devPi', 'pwd':'dpi' }  # RaspberryPi enezFLG chez AtelierZ
#DB_PARAMS = { 'host':'172.17.4.10', 'port':5432, 'base':'piBase', 'user':'devPi', 'pwd':'dpi' }    # OrangePi FLG enez 16Go a kerdale


