Système de dataloging domestique, installé sur un RaspberryPi, ne nécessitant pas de ressources Internet.

Système indépendant, modulaire et ouvert, pour faire du datalogging en interne (sans connexion Internet). Des connexions Internet sont toutefois possibles.
Matériel : Une carte OrangePiZero, qui peut servir de HotSpot WiFi, avec un SSD de 120Go  => coût d'environ 60€
avec une base de données PostgresQL, un service MQTT, et du évidemment un peu de code.

Pour les capteurs, la seule contrainte est qu'ils puissent envoyer leurs données vers le service MQTT. Un ESP8266 à moins de 5 € fait cela très bien. On peut imaginer des capteurs directement reliés à l'OrangePi, mais leurs données passeront systématiquement par le service MQTT. 
L'originalité de ce soft se trouve dans le fait que toutes les données doivent être au format json. Chaque capteur doit envoyer ses données ainsi formatées, ce qui permet au passage d'avoir des capteurs fournissant des données plus complètes (une trame complète téléinformation par exemple), Ceci va à l'encontre des préconisations et des bonnes pratiques de MQTT. Ici, c'est toutes la chaine de données qui est traitée ainsi. Même la persistance en base de données se fait au format json, ce qui justifie le choix du service de base de données : PostgresQL.
Le service MQTT est le point central de circulation des données, le hub. Le but est que tous les fournisseurs de données poussent leur données vers le service MQTT, et que tous les consommateurs des données courantes s'adressent à ce service pour les obtenir.
Il y a effectivement 2 types de données :
 - Les données courantes, ou la dernière donnée lue par le capteur, qui passent par le service MQTT
 - Les données historiques, l'ensemble des données du mois dernier par exemple, qui seront fournies par un service lié à la base de donnée
Les fournisseurs de données courantes peuvent être divers:
 - Un compteur d'électricité Linky
 - Un compteur d'énergie par impulsion
 - Un service tiers, par exemple associé à la base de donnée pour fournir une donnée agrégée, comme le consommation électrique des dernières 24 heures, ou l'écart entre consommation et production d'électricité sur les 10 dernières minutes, …
 - Un capteur température / hygrométrie
 - Une girouette / anémomètre
 - etc...
Les consommateurs de données courantes seront :
 - Une (ou plusieurs) base de données qui enregistrera tout ou partie des données
 - Un service d'analyse (ou plusieurs), Node-RED par exemple, permettant de déclencher des actions en fonction des données
 - Des page web affichant en temps réel les données issues des capteurs
 - Un service qui permet de sortir les données vers Internet
 - N'importe quoi qui serait intéressé par les données
Les données historiques seront accessibles via un service web interrogeant la base de données, sur le même style de ce que j'avais mis en place pour l'école de Kermelo. Une page web affichant des graphiques, ou un export des historiques utiliseront ce service de la même façon.

Le but est d'abord de voir si tout cela peut être paramétré facilement afin d'en rendre l'utilisation assez simple et ne nécessitant pas de code spécifique pour être mis en place.

Opération:
La première étape est d'installer l'Orange Pi, avec une base de données PostgresQL, un service MQTT, et les softs.
Il y a 2 softs :
 - Un site web qui va permettre de :
    - Configurer et de paramètrer l'ensemble.
    - Obtenir et afficher les données historiques
 - Un service d'enregistrement des données dans la base de données, qui récupère au fil de l'eau les données publiées sur le service MQTT
La seconde étape est la configuration :
 - accès à la base de données
 - Configuration du premier capteur, un compteur EDF par exemple. Cette configuration va permettre l'enregistrement des données
 - Configuration de l'afficage et mise à disposition du premier graphique.
Dés l'ajout du premier capteur, le service d'enregistrement se met en route et commence son travail
L'ajout de nouveaux capteurs se fera de la même façon.
