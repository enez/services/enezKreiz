#!/usr/bin/env python
# coding: utf-8

from app import app

# Debug
app.run( debug=True, host='0.0.0.0', port=5000 )
# Production
#app.run( host='0.0.0.0' )

# flask utility can be used (page 380)
#  export FLASK_APP=~yourProjets/Flask/configLog/runapp.py
#  export FLASK_DEBUG=1
#  flask run

# or with the terminal
#  python3 runapp.py
#
# in your browser :
#  http://127.0.0.1:5000
